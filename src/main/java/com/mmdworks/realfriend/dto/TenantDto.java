package com.mmdworks.realfriend.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class TenantDto {

	private long tenantId;

	private String tenantName;

	private String phone;

	private String addressLineOne;

	private String addressLineTwo;

	private String city;

	private String state;

	private String country;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date createdDate;

	private long propertyId;

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddressLineOne() {
		return addressLineOne;
	}

	public void setAddressLineOne(String addressLineOne) {
		this.addressLineOne = addressLineOne;
	}

	public String getAddressLineTwo() {
		return addressLineTwo;
	}

	public void setAddressLineTwo(String addressLineTwo) {
		this.addressLineTwo = addressLineTwo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public long getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(long propertyId) {
		this.propertyId = propertyId;
	}

	public long getTenantId() {
		return tenantId;
	}

	public void setTenantId(long tenantId) {
		this.tenantId = tenantId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TenantDto [tenantId=");
		builder.append(tenantId);
		builder.append(", tenantName=");
		builder.append(tenantName);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", addressLineOne=");
		builder.append(addressLineOne);
		builder.append(", addressLineTwo=");
		builder.append(addressLineTwo);
		builder.append(", city=");
		builder.append(city);
		builder.append(", state=");
		builder.append(state);
		builder.append(", country=");
		builder.append(country);
		builder.append(", createdDate=");
		builder.append(createdDate);
		builder.append(", propertyId=");
		builder.append(propertyId);
		builder.append("]");
		return builder.toString();
	}

}
