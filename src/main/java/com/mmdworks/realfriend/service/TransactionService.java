package com.mmdworks.realfriend.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mmdworks.realfriend.dao.PropertyRepository;
import com.mmdworks.realfriend.dao.TransactionRepository;
import com.mmdworks.realfriend.domain.Property;
import com.mmdworks.realfriend.domain.Transaction;
import com.mmdworks.realfriend.dto.TransactionDto;
import com.mmdworks.realfriend.enums.TransactionType;

@Service
public class TransactionService {

	@Autowired
	private TransactionRepository transactionRepository;

	@Autowired
	private PropertyRepository propertyRepository;

	@Autowired
	private ModelMapper modelMapper;

	public Transaction savetransaction(TransactionDto transactionDto) {
		System.out.println("-------------" + transactionDto.toString());
		Transaction transaction = null;
		transaction = transformToEntity(transactionDto);
		System.out.println("-------------" + transaction.toString());

		Property property = propertyRepository.findByPropertyId(transactionDto.getPropertyId());
		transaction.setProperty(property);
		return transactionRepository.save(transaction);

	}

	public List<Transaction> getTransactionsByProperty(long propertyId, String incomeType) {

		Property property = propertyRepository.findByPropertyId(propertyId);
		TransactionType transactionType = null;
		if (incomeType.equalsIgnoreCase("INCOME")) {
			transactionType = TransactionType.INCOME;
		} else if (incomeType.equalsIgnoreCase("EXPENSE")) {
			transactionType = TransactionType.EXPENSE;
		}
		return transactionRepository.getTransactionByPropertyAndTransactionType(property, transactionType);
	}
	
	public List<Transaction> getTransactionsByPropertyWithoutType(long propertyId) {

		Property property = propertyRepository.findByPropertyId(propertyId);
		
		
		return transactionRepository.getTransactionByProperty(property);
	}

	private Transaction transformToEntity(TransactionDto transactionDto) {
		return modelMapper.map(transactionDto, Transaction.class);
	}
}
