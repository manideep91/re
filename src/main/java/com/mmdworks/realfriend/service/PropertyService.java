package com.mmdworks.realfriend.service;

import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mmdworks.realfriend.dao.PropertyRepository;
import com.mmdworks.realfriend.dao.UserRepository;
import com.mmdworks.realfriend.domain.Property;
import com.mmdworks.realfriend.domain.User;
import com.mmdworks.realfriend.dto.PropertyDto;

@Service
public class PropertyService {
	@Autowired
	private PropertyRepository propertyRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ModelMapper modelMapper;

	public Property createProperty(PropertyDto propertyDto) {
		Property property = new Property();
		if (propertyDto != null) {

			User user = userRepository.findByUserId(propertyDto.getUser_id());
			property = transformToEntity(propertyDto);
			property.setUser(user);
			property.setCreatedDate(new Date());

		}
		return propertyRepository.save(property);
	}

	private Property transformToEntity(PropertyDto propertyDto) {
		return modelMapper.map(propertyDto, Property.class);
	}
	
	public List<Property> getAllProperties(){
		return (List<Property>) propertyRepository.findAll();
	}

}
