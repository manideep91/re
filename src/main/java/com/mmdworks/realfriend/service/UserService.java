package com.mmdworks.realfriend.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mmdworks.realfriend.dao.UserRepository;
import com.mmdworks.realfriend.domain.User;
import com.mmdworks.realfriend.enums.Status;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public User createuser(User user) {
		if (user != null) {
			user.setStatus(Status.CREATED);
			user.setCreatedDate(new Date());
		}
		return userRepository.save(user);
	}

	public List<User> getAllUsers() {
		return (List<User>) userRepository.findAll();
	}

	public List<User> getUserByEmail(String email) {
		return null;
	}

}
