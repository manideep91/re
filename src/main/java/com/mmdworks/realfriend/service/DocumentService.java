package com.mmdworks.realfriend.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mmdworks.realfriend.dao.DocumentRepository;
import com.mmdworks.realfriend.domain.Document;
import com.mmdworks.realfriend.enums.DocumentType;

@Service
public class DocumentService {

	@Autowired
	private DocumentRepository documentRepository;
	
	public Document saveDocument(MultipartFile inputFile,String category,String documentType) throws IOException{
		
		Document document=new Document();
		document.setDocumentType(DocumentType.RENTAL_DOC);
		
		File file=convert(inputFile);
        byte[] bFile = new byte[(int) file.length()];
        
        try {
   	     FileInputStream fileInputStream = new FileInputStream(file);
   	     //convert file into array of bytes
   	     fileInputStream.read(bFile);
   	     fileInputStream.close();
           } catch (Exception e) {
   	     e.printStackTrace();
           }
        
        document.setDocData(bFile);
        

//        try {
//         FileInputStream fileInputStream = new FileInputStream(file);
//         fileInputStream.read(bFile);
//         fileInputStream.close();
//        } catch (Exception e) {
//         e.printStackTrace();
//        }
		
	 return documentRepository.save(document);
		
	}
	
	public File convert(MultipartFile file) throws IOException
	{    
	    File convFile = new File(file.getOriginalFilename());
	    convFile.createNewFile(); 
	    FileOutputStream fos = new FileOutputStream(convFile); 
	    fos.write(file.getBytes());
	    fos.close(); 
	    return convFile;
	}
}
