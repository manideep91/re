package com.mmdworks.realfriend.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mmdworks.realfriend.dao.PropertyRepository;
import com.mmdworks.realfriend.dao.TenantRepository;
import com.mmdworks.realfriend.domain.Property;
import com.mmdworks.realfriend.domain.Tenant;
import com.mmdworks.realfriend.dto.TenantDto;

@Service
public class TenantService {
	
	@Autowired
	private TenantRepository tenantRepository;
	
	@Autowired
	private PropertyRepository propertyRepositry;

	@Autowired
	private ModelMapper modelMapper;
	
	
	public Tenant createTenant(TenantDto tenantDto){
		System.out.println("-------------"+tenantDto.toString());
		Tenant tenant=null;
		tenant=transformToEntity(tenantDto);
		System.out.println("-------------"+tenant.toString());

		Property property=propertyRepositry.findByPropertyId(tenantDto.getPropertyId());
		tenant.setProperty(property);
		return tenantRepository.save(tenant);
	}
	
	private Tenant transformToEntity(TenantDto tenantDto) {
		return modelMapper.map(tenantDto, Tenant.class);
	}
	
	public List<Tenant> getTenantsByProperty(long propertyId){
		
		Property property=propertyRepositry.findByPropertyId(propertyId);
		return tenantRepository.findByProperty(property);
	}
}
