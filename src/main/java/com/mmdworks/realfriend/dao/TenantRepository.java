package com.mmdworks.realfriend.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mmdworks.realfriend.domain.Property;
import com.mmdworks.realfriend.domain.Tenant;

public interface TenantRepository extends CrudRepository<Tenant,Long> {
	
	public List<Tenant> findByProperty(Property property);

}
