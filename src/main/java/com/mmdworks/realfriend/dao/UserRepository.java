
package com.mmdworks.realfriend.dao;

import org.springframework.data.repository.CrudRepository;

import com.mmdworks.realfriend.domain.User;

//This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
//CRUD refers Create, Read, Update, Delete
public interface UserRepository extends CrudRepository<User,Long>{

	public User findByUserId(long id);
}
