package com.mmdworks.realfriend.dao;

import org.springframework.data.repository.CrudRepository;

import com.mmdworks.realfriend.domain.Property;

public interface PropertyRepository extends CrudRepository<Property, Long>{
	
	public Property findByPropertyId(long id);
	

}
