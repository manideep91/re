package com.mmdworks.realfriend.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mmdworks.realfriend.domain.Property;
import com.mmdworks.realfriend.domain.Transaction;
import com.mmdworks.realfriend.enums.TransactionType;

public interface TransactionRepository extends CrudRepository<Transaction,Long> {
	
	public List<Transaction> getTransactionByPropertyAndTransactionType(Property property,TransactionType transactionType);
	
	public List<Transaction> getTransactionByProperty(Property property);

}
