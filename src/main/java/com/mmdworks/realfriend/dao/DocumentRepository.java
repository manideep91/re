package com.mmdworks.realfriend.dao;

import org.springframework.data.repository.CrudRepository;

import com.mmdworks.realfriend.domain.Document;

public interface DocumentRepository extends CrudRepository<Document, Long>{

}
