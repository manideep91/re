package com.mmdworks.realfriend.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "tenant")
public class Tenant {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long tenantId;

	@Column(name = "tenantName")
	private String tenantName;

	@Column(name = "phone")
	private String phone;

	@Column(name = "addLineOne")
	private String addressLineOne;

	@Column(name = "addLineTwo")
	private String addressLineTwo;

	@Column(name = "city")
	private String city;

	@Column(name = "state")
	private String state;

	@Column(name = "country")
	private String country;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@Column(name = "createdDate")
	private Date createdDate;

	@ManyToOne
	@JoinColumn(name = "property_id")
	@JsonIgnore
	private Property property;

	public long getTenantId() {
		return tenantId;
	}

	public void setTenantId(long tenantId) {
		this.tenantId = tenantId;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddressLineOne() {
		return addressLineOne;
	}

	public void setAddressLineOne(String addressLineOne) {
		this.addressLineOne = addressLineOne;
	}

	public String getAddressLineTwo() {
		return addressLineTwo;
	}

	public void setAddressLineTwo(String addressLineTwo) {
		this.addressLineTwo = addressLineTwo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Property getProperty() {
		return property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Tenant [tenantId=");
		builder.append(tenantId);
		builder.append(", tenantName=");
		builder.append(tenantName);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", addressLineOne=");
		builder.append(addressLineOne);
		builder.append(", addressLineTwo=");
		builder.append(addressLineTwo);
		builder.append(", city=");
		builder.append(city);
		builder.append(", state=");
		builder.append(state);
		builder.append(", country=");
		builder.append(country);
		builder.append(", createdDate=");
		builder.append(createdDate);
		builder.append(", property=");
		builder.append(property);
		builder.append("]");
		return builder.toString();
	}

}
