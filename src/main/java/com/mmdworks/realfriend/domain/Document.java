package com.mmdworks.realfriend.domain;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.mmdworks.realfriend.enums.DocumentType;

@Entity
@Table(name = "document")
public class Document {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name = "documentId")
	private long documentId;

	@Column(name = "description")
	private String descrption;

	@Column(name = "doc_type")
	@Enumerated(EnumType.STRING)
	private DocumentType documentType;

	@Column(name = "doc_data")
	@Lob
	private byte[] docData;
	
	@Column(name="propertyId")
	private long  propertyId;

	@Column(name="transactionId")
	private long transactionId;
	
	@Column(name="tenantId")
	private long tenantId;

	
	
	public long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(long documentId) {
		this.documentId = documentId;
	}

	public String getDescrption() {
		return descrption;
	}

	public void setDescrption(String descrption) {
		this.descrption = descrption;
	}

	public DocumentType getDocumentType() {
		return documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public byte[] getDocData() {
		return docData;
	}

	public void setDocData(byte[] docData) {
		this.docData = docData;
	}

	public long getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(long propertyId) {
		this.propertyId = propertyId;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getTenantId() {
		return tenantId;
	}

	public void setTenantId(long tenantId) {
		this.tenantId = tenantId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Document [documentId=");
		builder.append(documentId);
		builder.append(", descrption=");
		builder.append(descrption);
		builder.append(", documentType=");
		builder.append(documentType);
		builder.append(", docData=");
		builder.append(Arrays.toString(docData));
		builder.append(", propertyId=");
		builder.append(propertyId);
		builder.append(", transactionId=");
		builder.append(transactionId);
		builder.append(", tenantId=");
		builder.append(tenantId);
		builder.append("]");
		return builder.toString();
	}

	

}
