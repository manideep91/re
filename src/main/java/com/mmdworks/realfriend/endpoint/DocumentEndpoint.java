package com.mmdworks.realfriend.endpoint;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mmdworks.realfriend.domain.Document;
import com.mmdworks.realfriend.service.DocumentService;

@RestController
@RequestMapping("document-services")
public class DocumentEndpoint {

	@Autowired
	private DocumentService documentService;

	@PostMapping(value = "/save-document", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public @ResponseBody Document saveDocument(@RequestParam(required = true, value = "category") String category,
			@RequestParam(required = true, value = "documentType") String documentType, @RequestBody MultipartFile file)
			throws IOException {

		return documentService.saveDocument(file, category, documentType);

	}

}
