package com.mmdworks.realfriend.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mmdworks.realfriend.domain.Tenant;
import com.mmdworks.realfriend.dto.TenantDto;
import com.mmdworks.realfriend.service.TenantService;

@RestController
@RequestMapping("/tenant-service")
public class TenantEndPoint {
	
	@Autowired
	private TenantService tenantService;
	
	@PostMapping("/create-tenant")
	public @ResponseBody Tenant saveTenant(@RequestBody TenantDto tenantDto){
		return tenantService.createTenant(tenantDto);
	}
	
	@GetMapping("/tenants-by-property/{propertyId}")
	public List<Tenant> getTenantsByProperty(@PathVariable("propertyId")long propertyId){
		return tenantService.getTenantsByProperty(propertyId);
	}

}
