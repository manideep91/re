package com.mmdworks.realfriend.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mmdworks.realfriend.domain.Property;
import com.mmdworks.realfriend.dto.PropertyDto;
import com.mmdworks.realfriend.service.PropertyService;

@RestController
@RequestMapping("/property-services")
public class PropertyEndPoint {

	@Autowired
	private PropertyService propertyService;

	@PostMapping("/create-property")
	public @ResponseBody Property createUser(@RequestBody PropertyDto propertyDto) {
		Property property = new Property();
		try {
			property = propertyService.createProperty(propertyDto);
			property.getUser();
			System.out.println(property);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return property;

	}

	@GetMapping("/get-all-properties")
	public @ResponseBody List<Property> getAllProperties() {
		return propertyService.getAllProperties();
	}
}
