package com.mmdworks.realfriend.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mmdworks.realfriend.domain.User;
import com.mmdworks.realfriend.service.UserService;

@RestController
@RequestMapping("/user-services")
public class UserEndpoint {

	@Autowired
	private UserService userService;

	@GetMapping("/test")
	public String test() {
		return "Working";
	}

	@PostMapping("/create-user")
	public @ResponseBody User createUser(@RequestBody User user) {
		return userService.createuser(user);
	}

	@GetMapping("/get-all-users")
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}
}
