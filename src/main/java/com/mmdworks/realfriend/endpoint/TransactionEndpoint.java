package com.mmdworks.realfriend.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.mmdworks.realfriend.domain.Transaction;
import com.mmdworks.realfriend.dto.TransactionDto;
import com.mmdworks.realfriend.service.TransactionService;

@RestController
@RequestMapping(value="/trans-service")
public class TransactionEndpoint {
	
	@Autowired
	private TransactionService transactionService;

	@PostMapping(value="/create-transaction")
	public @ResponseBody Transaction saveTransaction(@RequestBody TransactionDto transactionDto){
		return transactionService.savetransaction(transactionDto);
	}
	
	@GetMapping(value="/get-trans-by-prop/{propertyId}/{type}")
	public @ResponseBody List<Transaction> getTransactionsByProperty(@PathVariable("propertyId")long propertyId,
			@PathVariable("type")String type){
		return transactionService.getTransactionsByProperty(propertyId, type);
	}
	@GetMapping(value="/get-transactions/{propertyId}")
	public @ResponseBody List<Transaction> getTransactionsByPropertyWithoutType(@PathVariable("propertyId")long propertyId){
		return transactionService.getTransactionsByPropertyWithoutType(propertyId);
	}
}
