package com.mmdworks.realfriend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RealfriendApplication {

	public static void main(String[] args) {
		SpringApplication.run(RealfriendApplication.class, args);
	}
}
